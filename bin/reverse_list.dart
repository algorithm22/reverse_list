import 'dart:io';

void main(List<String> arguments) {
  var num = [];
  String? n = stdin.readLineSync()!;
  Stopwatch stopwatch = new Stopwatch()..start();
  var lst = n.split(" ");
  for (int i = 0; i < 10; i++) {
    int x = int.parse(lst[i]);
    num.add(x);
  }
  print("Before");
  for (int j = 0; j < num.length; j++) {
    stdout.write(num[j]);
    stdout.write(" ");
  }
  print("");
  print("After");
  for (int k = num.length - 1; k >= 0; k--) {
    stdout.write(num[k]);
    stdout.write(" ");
  }
  print("");
  print('Running time to executed = ${stopwatch.elapsedMilliseconds} ms');
}
